export class Constains {
  public static METHODS = [
    {
      name: 'POST',
      value: 'POST'
    },
    {
      name: 'GET',
      value: 'GET'
    },
    {
      name: 'PUT',
      value: 'PUT'
    }
  ];
  public static DOMAIN = 'http://10.0.0.80:9000';
  public static API_LIST_BATCH = '/api/batching/list';
  public static API_BATCH_HISTORY = 'api/batching/list/history';
  public static API_BATCH_HISTORY_INSERT = 'api/batching/list/history/insert';
  public static API_ADD_BATCH = 'api/batching/insert';
}
