import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CreateHeaderOption, createRequestOption} from '../request/request-util';


@Injectable({
  providedIn: 'root'
})
export class ApiUtilService {

  constructor(private http: HttpClient) { }
  delete(url: any): Observable<HttpResponse<{}>> {
    console.log(url);
    const headers =  CreateHeaderOption.getHeader();
    return this.http.delete(url, {headers: headers, observe: 'response'});
  }
  query(resourceUrl: any, req?: any): Observable<any> {
    const options = createRequestOption(req);
    const headers =  CreateHeaderOption.getHeader();
    return this.http.get<any[]>(resourceUrl, {  headers: headers, params: options, observe: 'response' });
  }
  create(resourceUrl: any, value: any, req?: any): Observable<any> {
    const options = createRequestOption(req);
    const headers =  CreateHeaderOption.getHeader();
    return this.http.post(resourceUrl , value, { headers });
  }
  update(resourceUrl: string, data: any): Observable<any> {
    console.log(data);
    return this.http.put<any>(resourceUrl + `/${data.id}`, data, { observe: 'response' });
  }
  get(resourceUrl: string, req?: any): Observable<any> {
    const options = createRequestOption(req);
    const headers =  CreateHeaderOption.getHeader();
    return this.http.get<any>(resourceUrl, {  headers: headers, params: options, observe: 'response' });
  }


}
