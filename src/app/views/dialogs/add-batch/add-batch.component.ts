import { Component, OnInit } from '@angular/core';
import {Constains} from "../../../../constains";
import {FormBuilder, Validators} from "@angular/forms";
import {ApiUtilService} from "../../../util/api-util.service";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-add-batch',
  templateUrl: './add-batch.component.html',
  styleUrls: ['./add-batch.component.scss']
})
export class AddBatchComponent implements OnInit {
  form: any;
  methods = Constains.METHODS;
  resourceUrl = `${Constains.DOMAIN}/${Constains.API_ADD_BATCH}`
  constructor(private fb: FormBuilder,
              private activeModal: NgbActiveModal,
              private api: ApiUtilService) { }

  ngOnInit(): void {
    this.form =  this.fb.group({
      apiName: ['', [Validators.required]],
      url: ['', [Validators.required]],
      apiMethod: 'GET',
      hasParam: true,
      warningMessage: ['', [Validators.required]]
    });
  }

  cancel() {

  }

  save() {
    this.form.value['requestParam'] = this.form.value['hasParam'] ? 'Y' : 'N';
    console.log(this.form);
    // return;
    this.api.create(this.resourceUrl, this.form.value).subscribe(
      res => {
        console.log(res);
        this.activeModal.dismiss();
        alert("Thêm batch thành công");
      }, error => {
        console.log(error)
        alert("Thêm batch thất bại");
      }
    )
  }
}
