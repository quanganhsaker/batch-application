import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-input-json',
  templateUrl: './input-json.component.html',
  styleUrls: ['./input-json.component.scss']
})
export class InputJsonComponent implements OnInit {
  @Input() data: any;
   newParams: any;
  parseError: boolean = false;
  // change events from the textarea
  public onChange(event: any) {

    // get value from text area
    this.newParams = event.target.value;
    try {
      // parse it to json
      JSON.parse(this.newParams);
      this.parseError = false;
    } catch (ex) {
      // set parse error if it fails
      this.parseError = true;
    }
  }
  constructor(
    protected activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
    this.newParams = JSON.stringify(this.data)
  }
  save() {
    console.log(this.data);
    this.data = this.newParams;
    console.log(this.data)
    this.activeModal.close(JSON.parse(this.data))

  }

}
