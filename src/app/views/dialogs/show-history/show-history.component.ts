import {Component, Input, OnInit} from '@angular/core';
import {ApiUtilService} from "../../../util/api-util.service";
import {Constains} from "../../../../constains";
import {JSONFile} from "@angular/cli/utilities/json-file";

@Component({
  selector: 'app-show-history',
  templateUrl: './show-history.component.html',
  styleUrls: ['./show-history.component.scss']
})
export class ShowHistoryComponent implements OnInit {

  @Input() id: number | undefined;
  historys = [];
  JSON = JSON;
  constructor(private api: ApiUtilService) { }
  ngOnInit(): void {
    this.api.get(`${Constains.DOMAIN}/${Constains.API_BATCH_HISTORY}?batchId=${this.id}`).subscribe(
      res => {
        this.historys = res.body.data;
        console.log(this.historys)
      }, err => {
        console.log(err)
      }
    )

  }

}
