import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Constains} from "../../../../constains";
import {NgxSpinnerService} from "ngx-spinner";
import {ApiUtilService} from "../../../util/api-util.service";

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  @Input() batch: any;
  methods = Constains.METHODS;
  constructor(
    private http: HttpClient,
    protected activeModal: NgbActiveModal,
    private spinner: NgxSpinnerService,
    private api: ApiUtilService
  ) { }

  ngOnInit(): void {
  }

  close() {
    this.activeModal.dismiss();
  }

  confirm() {
    this.spinner.show();
    console.log(this.batch)
    console.log(this.http);
    let request: any;
    switch (this.batch.apiMethod) {
      case 'POST':

        request = this.http.post(this.batch.url, this.batch.params);
        break;
      case 'GET' :
        request = this.http.get(this.batch.url, this.batch.params);
        break;
      case 'PUT':
        request = this.http.put(this.batch.url, this.batch.params)
        break;
    }
    let params = {
      batchId: this.batch.id,
      responseStatus: this.batch.status,
      responseBody: this.batch.response,
      inputParam: ''
    }

    request.subscribe(
        (res: any) => {
        console.log(res);
        this.batch.status = 'SUCCESS';
        params.responseStatus = 'SUCCESS'
        this.batch.response = res;
        params.responseBody = JSON.stringify(res);
          this.api.create(`${Constains.DOMAIN}/${Constains.API_BATCH_HISTORY_INSERT}`, params).subscribe(
            res => {
              console.log('insert thanh cong' ,res);
            }, err => {
              console.log('insert that bai', err);
            }
          )
        this.activeModal.dismiss();

          this.spinner.hide();
      }, (err: { message: any; }) => {
        console.log(err);
        this.batch.status = 'FAILURE';
        params.responseStatus = 'FAILURE';
        params.responseBody = JSON.stringify(err);
        this.api.create(`${Constains.DOMAIN}/${Constains.API_BATCH_HISTORY_INSERT}`, params).subscribe(
          res => {
            console.log('insert thanh cong' ,res);
          }, err => {
            console.log('insert that bai', err);
          }
        )
        this.batch.response = err.message;
        this.activeModal.dismiss();
        this.spinner.hide();
      }

    )


  }
}
