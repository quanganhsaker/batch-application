import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-show-batch',
  templateUrl: './show-batch.component.html',
  styleUrls: ['./show-batch.component.scss']
})
export class ShowBatchComponent implements OnInit {
  @Input() batch: any;
  JSON = JSON;

  constructor(
    protected activeModal: NgbActiveModal,
  ) { }

  ngOnInit(): void {
  }

  cancel() {
    this.activeModal.dismiss()
  }
}
