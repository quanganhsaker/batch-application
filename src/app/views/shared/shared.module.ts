import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableCommonComponent } from './table-common/table-common.component';



@NgModule({
    declarations: [
        TableCommonComponent
    ],
    exports: [
        TableCommonComponent
    ],
    imports: [
        CommonModule
    ]
})
export class SharedModule { }
