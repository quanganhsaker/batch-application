import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-table-common',
  templateUrl: './table-common.component.html',
  styleUrls: ['./table-common.component.scss']
})
export class TableCommonComponent implements OnInit {

  @Input() headers: any;
  @Input() attrs: any;
  @Input() data_from_api: any;
  @Input() url: any;
  @Input() detail: any;
  @Input() attr_get_detail: any;
  @Input() actions: any;
  @Input() deleteUrl: any;
  @Input() editUrl: any;
  @Input() messageDelete: any;
  @Input() dataSelected: any;
  @Input() numberStickyCol: number | undefined;
  @Output() change: EventEmitter<any> = new EventEmitter<any>();
  total: any;
  currentPage = 0;
  size = 5;
  selectedRow: any;
  HighlightRow = -1;
  constructor(
  ) {
  }
  ngOnInit(): void {
    if (!this.numberStickyCol) { this.numberStickyCol = 0; }
    console.log(this.data_from_api);
  }



}
