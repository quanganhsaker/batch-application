import { Component, OnInit } from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ConfirmComponent} from "../dialogs/confirm/confirm.component";
import {InputJsonComponent} from "../dialogs/input-json/input-json.component";
import {ShowBatchComponent} from "../dialogs/show-batch/show-batch.component";
import {Constains} from "../../../constains";
import {AddBatchComponent} from "../dialogs/add-batch/add-batch.component";
import {ShowHistoryComponent} from "../dialogs/show-history/show-history.component";
import {ApiUtilService} from "../../util/api-util.service";

@Component({
  selector: 'app-batch-view',
  templateUrl: './batch-view.component.html',
  styleUrls: ['./batch-view.component.scss']
})
export class BatchViewComponent implements OnInit {
  methods = Constains.METHODS;
  constructor(protected modalService: NgbModal,
              private api: ApiUtilService) { }
  resourceUrl = Constains.DOMAIN + Constains.API_LIST_BATCH;
  JSON = JSON;
  listBatch = [];
  method = 'post';
  ngOnInit(): void {
    this.api.get(this.resourceUrl).subscribe(
      res => {
        this.listBatch = res.body.data;
        for(const batch of this.listBatch) {
          // @ts-ignore
          batch['response'] = '';

        }
        this.listBatch = this.listBatch.filter(batch => {return batch['status'] !== 'N'});
      }, err => {
        console.log(err)
      }
    )
  }

  openConfirm(batch: any) {
    const modalRef = this.modalService.open(ConfirmComponent , {windowClass : 'myCustomModalClass'}  );
    modalRef.componentInstance.batch = batch;
  }

  addParam(batch: any) {
    const modalRef = this.modalService.open(InputJsonComponent, {windowClass: 'myCustomModalClass'});
    modalRef.componentInstance.data = batch['params'];
    modalRef.closed.subscribe(
      data => {
        batch.params = data;
        console.log(batch['params'])
      }
    )
  }

  showBatch(batch: any) {
    const modalRef = this.modalService.open(ShowBatchComponent, {windowClass: 'myCustomModalClass'});
    modalRef.componentInstance.batch =batch
  }

  changeMethod(event: any, batch: any) {
    console.log(event.target.value)
    batch.method = event.target.value;
  }

  addBatch() {
    const modalRef = this.modalService.open(AddBatchComponent, {windowClass: ''});
  }

  showHistory(batch: any) {
    const modalRef = this.modalService.open(ShowHistoryComponent, {size: "lg",windowClass: 'myCustomModalClass'});
    modalRef.componentInstance.id = batch.id

  }
}
