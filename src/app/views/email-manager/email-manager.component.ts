import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-email-manager',
  templateUrl: './email-manager.component.html',
  styleUrls: ['./email-manager.component.scss']
})
export class EmailManagerComponent implements OnInit {
  headers = [ 'Hệ thống', 'Dự án', 'Mã biểu mẫu', 'Tên biểu mẫu', 'Tiêu đề email', 'Trạng thái'];
  attrs = [
    {
      name: 'system',
      type: 'text'
    },
    {
      name: 'projectName',
      type: 'text'
    }, {
      name: 'code',
      type: 'text'
    }, {
      name: 'name',
      type: 'text'
    }, {
      name: 'subject',
      type: 'text'
    }, {
      name: 'status',
      type: 'status'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
